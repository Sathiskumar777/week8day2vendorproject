package runner;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.SnippetType;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(features = "src/test/java/feature/vendor.feature", glue="TCSteps", monochrome = true, plugin= {"pretty", "html:reports/cucmberBasrreport"}/*dryRun=true, snippets = SnippetType.CAMELCASE*/)
public class RunTest {

}
