package classWork;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.Test;

public class Vendors {

	@Test
	public void vendorName() {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.get("https://acme-test.uipath.com/account/login");

		driver.findElementById("email").sendKeys("Sathiskumar777@gmail.com");
		driver.findElementById("password").sendKeys("ramsruk@90");

		driver.findElementById("buttonLogin").click();
		
		
		WebElement mouseOver = driver.findElementByXPath("(//button[@class = 'btn btn-default btn-lg'])[4]");
		Actions builder = new Actions(driver);
		Action build = builder.moveToElement(mouseOver).build();
		build.perform();

		driver.findElementByLinkText("Search for Vendor").click();

		driver.findElementById("vendorTaxID").sendKeys("RO212121");
		driver.findElementById("buttonSearch").click();

		String vendor = driver.findElementByXPath("(//table[@class = 'table']//tr[2])/td").getText();
		System.out.println("The Vendor name is "+vendor);
	}

}
