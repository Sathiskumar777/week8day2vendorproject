package TCSteps;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class VendorName {
	ChromeDriver driver;
	@Given("Open the Browser")
	public void openTheBrowser() {
	    System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
	    driver= new ChromeDriver();
	}

	@Given("Maximise")
	public void maximiseTheBrowser() {
	    driver.manage().window().maximize();
	}

	@Given("set the Timeout")
	public void setTheTimeout() {
	    driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	}

	@Given("Load the URL")
	public void loadTheURL() {
	    driver.get("https://acme-test.uipath.com/account/login");
	}

	@Given("Enter Username")
	public void enterUsername() {
		driver.findElementById("email").sendKeys("Sathiskumar777@gmail.com");
	}

	@Given("Enter Password")
	public void enterPassword() {
		driver.findElementById("password").sendKeys("ramsruk@90");
	}

	@When("click Login")
	public void clickLogin() {
		driver.findElementById("buttonLogin").click();
	}

	@When("Mouse over the Vendors option")
	public void mouseOverTheVendorsOption() {
		WebElement mouseOver = driver.findElementByXPath("(//button[@class = 'btn btn-default btn-lg'])[4]");
		Actions builder = new Actions(driver);
		Action build = builder.moveToElement(mouseOver).build();
		build.perform();
	}

	@When("click on Search for Vendor")
	public void clickOnSearchForVendor() {
		driver.findElementByLinkText("Search for Vendor").click();
	}

	@Then("Enter TAXid")
	public void enterTAXid() {
		driver.findElementById("vendorTaxID").sendKeys("RO212121");
	}

	@When("click on Search button")
	public void clickOnSearchButton() {
		driver.findElementById("buttonSearch").click();
	}

	@Then("Print the Vendor name")
	public void printTheVendorName() {
		String vendor = driver.findElementByXPath("(//table[@class = 'table']//tr[2])/td").getText();
		System.out.println("The Vendor name is "+vendor);
	}



}
