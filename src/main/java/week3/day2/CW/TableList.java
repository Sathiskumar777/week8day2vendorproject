package week3.day2.CW;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class TableList {

	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		
		driver.get("http://erail.in");
		driver.findElementById("txtStationFrom").clear();
		driver.findElementById("txtStationFrom").sendKeys("MAS", Keys.TAB);
		driver.findElementById("txtStationTo").clear();
		driver.findElementById("txtStationTo").sendKeys("CVP",Keys.TAB);
		boolean selected = driver.findElementById("chkSelectDateOnly").isSelected();
		if(selected) {
			driver.findElementById("chkSelectDateOnly").click();
		}
		Thread.sleep(2000);
		
		WebElement trainTable = driver.findElementByXPath("//table[@class = 'DataTable TrainList']");
		List<WebElement> trainRow = trainTable.findElements(By.tagName("tr"));
		//trainRow.size();
		for(int i=trainRow.size()-1;i>=0;i--) {
			WebElement eachRow = trainRow.get(i);
			List<WebElement> trainColumn = eachRow.findElements(By.tagName("td"));
			String trainList = trainColumn.get(1).getText();
			System.out.println(trainList);
		}
		

	}

}
