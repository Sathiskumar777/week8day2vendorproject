package week3.day2.HW;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.Test;

public class DuplicateLead {
@Test
public void Duplicate() throws InterruptedException {
	//public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver","./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver(); 
		driver.get("http://leaftaps.com/opentaps/");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
		
		driver.findElementByXPath("//input[@id = 'username']").sendKeys("DemoSalesManager");
		driver.findElementByXPath("//input[@id = 'password']").sendKeys("crmsfa");
		driver.findElementByXPath("//input[@class = 'decorativeSubmit']").click();
		
		driver.findElementByXPath("//a[contains(text(), 'CRM/SFA')]").click();
		
		driver.findElementByXPath("//a[text() = 'Leads']").click();
		
		driver.findElementByXPath("//a[contains(text(),'Find Leads')]").click();
		
		driver.findElementByXPath("//span[text() = 'Email']").click();
		
		driver.findElementByXPath("//input[@name = 'emailAddress']").sendKeys("demo@demolead1.com");
		
		driver.findElementByXPath("//button[contains(text(), 'Find Leads')]").click();
		Thread.sleep(3000);
		
		WebElement name = driver.findElementByXPath("((((((//div[@class = 'x-grid3-cell-inner x-grid3-col-partyId']/child::a)[1])/../..)/following-sibling::td)[2])//a)");
		String leadName = name.getText();
		System.out.println(leadName);
		
		driver.findElementByXPath("((//div[@class = 'x-grid3-cell-inner x-grid3-col-partyId']/child::a)[1])").click();
		//Actions act = new Actions(driver);
		//WebElement element = driver.findElementByXPath("((//div[@class = 'x-grid3-cell-inner x-grid3-col-partyId']/child::a)[1])");
		//act.moveToElement(element).click().perform();		
		
		driver.findElementByXPath("//a[contains(text(), 'Duplicate Lead')]").click();
		
		WebElement titleName = driver.findElementByXPath("//div[text() = 'Duplicate Lead']");
		String title = titleName.getText();
		System.out.println("The title name of the page is "+title);
		
		driver.findElementByXPath("//input[@name = 'submitButton']").click();
		Thread.sleep(2000);
		
		WebElement duplicate = driver.findElementByXPath("//span[@id = 'viewLead_firstName_sp']");
		String dupName = duplicate.getText();
		if (dupName.equals(leadName)){
			System.out.println("Duplicated name is same as original lead name");
		}else {
			System.out.println("Duplicated name is not same as original lead name");
			
		}
		
		
}
}