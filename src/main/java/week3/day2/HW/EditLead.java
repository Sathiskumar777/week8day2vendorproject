package week3.day2.HW;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

public class EditLead {
@Test(dependsOnMethods="week3.day2.HW.DuplicateLead.Duplicate", invocationCount =2, threadPoolSize =2)
public void Edit() {
	//public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver","./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver(); 
		driver.get("http://leaftaps.com/opentaps/");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
		
		driver.findElementByXPath("//input[@id = 'username']").sendKeys("DemoSalesManager");
		driver.findElementByXPath("//input[@id = 'password']").sendKeys("crmsfa");
		driver.findElementByXPath("//input[@class = 'decorativeSubmit']").click();
		
		driver.findElementByXPath("//a[contains(text(), 'CRM/SFA')]").click();
		
		driver.findElementByXPath("//a[text() = 'Leads']").click();
		
		driver.findElementByXPath("//a[contains(text(),'Find Leads')]").click();
		
		driver.findElementByXPath("(//input[@name = 'firstName'])[3]").sendKeys("gayu");
		
		driver.findElementByXPath("//button[text()='Find Leads']").click();
		
		driver.findElementByXPath("(//div[@class = 'x-grid3-cell-inner x-grid3-col-partyId']/child::a)[1]").click();
		
		WebElement pageTitle = driver.findElementByXPath("//div[text() = 'View Lead']");
		String pageTitleName = pageTitle.getText();
		System.out.println("Title of the page name is "+pageTitleName);
		
		driver.findElementByXPath("//a[text() = 'Edit']").click();
		
		driver.findElementByXPath("//input[@id = 'updateLeadForm_companyName']").clear();
		driver.findElementByXPath("//input[@id = 'updateLeadForm_companyName']").sendKeys("TestLeaf");
		
		driver.findElementByXPath("//input[@name = 'submitButton']").click();
		
		boolean displayed = driver.findElementByXPath("//span[contains(text(), 'Leaf')]").isDisplayed();
		if(displayed == true) {
			System.out.println("Company name is updated");
		}else {
			System.out.println("Company name is not updated");
				
		}
		
		driver.close();

	}

}
