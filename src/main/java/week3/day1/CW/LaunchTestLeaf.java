package week3.day1.CW;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class LaunchTestLeaf {

	public static void main(String[] args) {
	//Launch browser
		System.setProperty("webdriver.chrome.driver","./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver(); 
	//Maximize the browser
	driver.manage().window().maximize();
	//load url
		driver.get("http://leaftaps.com/opentaps/");
	//username
		driver.findElementById("username").sendKeys("DemoSalesManager");
	//password
		driver.findElementById("password").sendKeys("crmsfa");
	//login
		driver.findElementByClassName("decorativeSubmit").click();
	//click crmfsa
		driver.findElementByLinkText("CRM/SFA").click();
	//click create lead
		driver.findElementByLinkText("Create Lead").click();
	//Filling Mandatory fields
		driver.findElementById("createLeadForm_companyName").sendKeys("TestLeaf");
		driver.findElementById("createLeadForm_firstName").sendKeys("Sathis kumar");
		driver.findElementById("createLeadForm_lastName").sendKeys("Ramasamy");
	//	driver.findElementByName("submitButton").click();
	//Identifying the 'Source' Dropdown field 
		WebElement source = driver.findElementById("createLeadForm_dataSourceId");
	//Selecting the value by using source object above
		Select sel = new Select(source);
		sel.selectByVisibleText("Employee");
	//Identifying the 'Marketing Campaign' Dropdown field 
		WebElement marketing = driver.findElementById("createLeadForm_marketingCampaignId");
	//Selecting the value by using marketing object above <selectByValue>
		Select mar = new Select(marketing);
		mar.selectByValue("CATRQ_CARNDRIVER");
	//Identifying the 'Industry' Dropdown field 
		WebElement industry = driver.findElementById("createLeadForm_industryEnumId");
	//Getting the values starts with M from the list
		Select ind = new Select(industry);
		List<WebElement> options = ind.getOptions();
		for (WebElement eachOption : options) {
			if((eachOption.getText()).startsWith("A")) {
			//	System.out.println(letterM.getText());
				ind.selectByVisibleText(eachOption.getText());
	//Filling all other fieldes
		driver.findElementById("createLeadForm_firstNameLocal").sendKeys("Sathis");
		driver.findElementById("createLeadForm_personalTitle").sendKeys("Dear");
		driver.findElementById("createLeadForm_generalProfTitle").sendKeys("Mr");
		driver.findElementById("createLeadForm_annualRevenue").sendKeys("300000");
		
		WebElement owner = driver.findElementById("createLeadForm_ownershipEnumId");
		Select own = new Select(owner);
		own.selectByVisibleText("Partnership");
		
		driver.findElementById("createLeadForm_sicCode").sendKeys("625896");
		driver.findElementById("createLeadForm_description").sendKeys("Selenium testing classes");
		driver.findElementById("createLeadForm_importantNote").sendKeys("Nothing");
		//driver.findElementById("createLeadForm_parentPartyId").sendKeys("P12345");
		driver.findElementById("createLeadForm_lastNameLocal").sendKeys("kumar");
		driver.findElementById("createLeadForm_birthDate").sendKeys("01/26/19");
		driver.findElementById("createLeadForm_departmentName").sendKeys("CSE");
		
		WebElement currency = driver.findElementById("createLeadForm_currencyUomId");
		Select curr = new Select(currency);
		List<WebElement> eachCurr = curr.getOptions();
		for (WebElement currINR : eachCurr) {
			if(currINR.getText().startsWith("INR")){
				curr.selectByVisibleText(currINR.getText());
			}
			
		}
		
		
	//close browser
		//	driver.close();
			}
			
		}
		
		
		
	}

}
