package week3.day1.HW;

import org.openqa.selenium.chrome.ChromeDriver;

public class AutomateEditAction {
	
public void Edit() {
	System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
	ChromeDriver driver = new ChromeDriver();
	driver.manage().window().maximize();
	driver.get("http://www.leafground.com/pages/Edit.html");
}

	public static void main(String[] args) {
		AutomateEditAction action = new AutomateEditAction();
		action.Edit();
	}

}
