package week3.day1.HW;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class IRCTCSignup {

	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://www.irctc.co.in/eticketing/userSignUp.jsf");

		driver.findElementById("userRegistrationForm:userName").sendKeys("sathisya123");
		driver.findElementById("userRegistrationForm:password").sendKeys("RamsRuk@90");
		driver.findElementById("userRegistrationForm:confpasword").sendKeys("RamsRuk@90");
		
		WebElement security = driver.findElementById("userRegistrationForm:securityQ");
		Select secur = new Select(security);
		secur.selectByVisibleText("What is your pet name?");
		
		driver.findElementById("userRegistrationForm:securityAnswer").sendKeys("Ice");
		
		WebElement language = driver.findElementById("userRegistrationForm:prelan");
		Select lang = new Select(language);
		lang.selectByVisibleText("English");
		
		driver.findElementById("userRegistrationForm:firstName").sendKeys("Sathis kumar");
		driver.findElementById("userRegistrationForm:lastName").sendKeys("Ramasamy");
		driver.findElementByName("userRegistrationForm:gender").click();
		driver.findElementByName("userRegistrationForm:maritalStatus").click();
		
		WebElement date = driver.findElementById("userRegistrationForm:dobDay");
		Select dat = new Select(date);
		dat.selectByValue("24");
		WebElement month = driver.findElementById("userRegistrationForm:dobMonth");
		Select mon = new Select(month);
		mon.selectByVisibleText("APR");
		WebElement year = driver.findElementById("userRegistrationForm:dateOfBirth");
		Select yea = new Select(year);
		yea.selectByValue("1990");
		
		WebElement occupation = driver.findElementById("userRegistrationForm:occupation");
		Select occupat = new Select(occupation);
		occupat.selectByVisibleText("Government");
		
		WebElement country = driver.findElementById("userRegistrationForm:countries");
		Select count = new Select(country);
		count.selectByVisibleText("India");
		Thread.sleep(3000);
		
		driver.findElementById("userRegistrationForm:email").sendKeys("Sathiskumar@gmail.com");
		driver.findElementById("userRegistrationForm:mobile").sendKeys("7358277856");
		
		WebElement nationality = driver.findElementById("userRegistrationForm:nationalityId");
		Select nation = new Select(nationality);
		nation.selectByVisibleText("India");
		
		driver.findElementById("userRegistrationForm:address").sendKeys("8, Sakthi enclave");
		driver.findElementById("userRegistrationForm:area").sendKeys("Tambaram Sanatorium");
		driver.findElementById("userRegistrationForm:pincode").sendKeys("628303", Keys.TAB);
		Thread.sleep(3000);
		
		
		WebElement cityName = driver.findElementById("userRegistrationForm:cityName");
		Select city = new Select(cityName);
		city.selectByVisibleText("Tuticorin");
		
		WebElement postoffice = driver.findElementById("userRegistrationForm:postofficeName");
		Select post = new Select(postoffice);
		post.selectByIndex(3);
		
		driver.findElementById("userRegistrationForm:landline").sendKeys("044230523");
		
		driver.findElementByName("userRegistrationForm:resAndOff").click();		

	}

}
