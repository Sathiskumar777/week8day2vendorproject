package codingChallenge;

import java.util.Scanner;

public class CC17 {

	public static void main(String[] args) {
		System.out.println("Enter the sentence");
		Scanner sc = new Scanner(System.in);
		String str = sc.nextLine();
		int strLen = str.length();
		int countUpper=0,countLower=0,countDigit=0,countSpl=0;
		float percentUpper,percentLower,percentDigit, percentSpl;
		
		char[] charArray = str.toCharArray();
		for(char c:charArray) {
			if(Character.isUpperCase(c))
				countUpper++;
			if(Character.isLowerCase(c))
				countLower++;
			if(Character.isDigit(c))
				countDigit++;
			if(!Character.isDigit(c) && !Character.isAlphabetic(c))
				countSpl++;	
			}
		percentUpper =(float) (countUpper*100)/strLen;
		System.out.println("Num of Uppercase letters is "+countUpper+", So percentage is "+percentUpper+"%");
		percentLower = (float) (countLower*100)/strLen;
		System.out.println("Num of Lowercase letters is "+countLower+", So percentage is "+percentLower+"%");
		percentDigit = (float) (countDigit*100)/strLen;
		System.out.println("Num of case letters is "+countDigit+", So percentage is "+percentDigit+"%");
		percentSpl = (float) (countSpl*100)/strLen;
		System.out.println("Num of Uppercase letters is "+countSpl+", So percentage is "+percentSpl+"%");
		}

	}
