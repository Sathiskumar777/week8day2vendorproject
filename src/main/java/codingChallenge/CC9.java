package codingChallenge;
import java.util.Scanner;
public class CC9 {

	public static void main(String[] args) {
		System.out.println("Enter the string");
		Scanner sc = new Scanner(System.in);
		String org = sc.nextLine();
		String rev = "";
		int length = org.length();
		for(int i=length-1;i>=0;i--) {
			rev = rev+org.charAt(i);
		}
		if(org.equals(rev)){
		System.out.println(org+" is a Palindrome");
		}else {
		System.out.println(org+" is not a Palindrome");
		}
	}

}
