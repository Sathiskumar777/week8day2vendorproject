package codingChallenge;

import java.util.Scanner;

public class CC21 {

	public static void main(String[] args) {
	//	int row,col,i,j;
		int arr[][] = new int[10][10];
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Enter the Number of rows");
		int row = sc.nextInt();
		System.out.println("Enter the number of columns");
		int col = sc.nextInt();
		
		System.out.println("The eleents are:");
		for(int i=0;i<row;i++) {
			for(int j=0; j<col;j++) {
				arr[i][j] = sc.nextInt();
				System.out.println();
			}
		}
		
		System.out.println("Output matrix is:");
		for(int i=0;i<row;i++) {
			for(int j=0; j<col;j++) {
				System.out.print(arr[i][j]+" ");
			}
			System.out.println();
		}

	}

}
