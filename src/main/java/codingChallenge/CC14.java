
// * ***********Floyd triangle code***************
package codingChallenge;
import java.util.Scanner;

public class CC14 {

	public static void main(String[] args) {
		System.out.println("Enter the row number");
		Scanner sc = new Scanner(System.in);
		int rowNum = sc.nextInt();
		int a=1;
		for(int i=1;i<=rowNum;i++) {
			for(int j=1;j<=i;j++) {
			System.out.print(a);
			a++;
		}
		System.out.println();
		}
	}

}


/*
 * **************Dropdown task**************** 
 package codingChallenge;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class CC14 {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://testleaf.herokuapp.com/");
		
		driver.findElementByLinkText("Drop down").click();
		
		WebElement selectDrop = driver.findElementByXPath("(//option[contains(text(), 'Selenium')])/..");
		Select sel = new Select(selectDrop);
		List<WebElement> options = sel.getOptions();
		sel.selectByIndex(options.size()-2);
		System.out.println(sel);		
	}
}
*/


/*
 * ****************Checkbox task ****************************************

package codingChallenge;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class CC14{
	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://testleaf.herokuapp.com/");
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		
		driver.findElementByLinkText("Checkbox").click();
		
		WebElement select = driver.findElementByXPath("//label[contains(text(), 'Confirm Selenium is checked')]/following-sibling::input");
		
		if(select.isSelected()) {
			System.out.println("Checked");
		}else {
			System.out.println("unchecked");
		}
		
		if (driver.findElementByXPath("//label[contains(text(), 'DeSelect only checked')]/following-sibling::input").isSelected()) {
			System.out.println("checked");
		}else {
			System.out.println("unchecked");
		}
			
	}
}
*/

