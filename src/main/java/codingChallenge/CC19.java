package codingChallenge;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Scanner;
import java.util.Set;

public class CC19 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the Number");
		int num = sc.nextInt();
		System.out.println("Enter the Digit");
		int digit = sc.nextInt();
		int orgNum = 0; 
		while((Integer.toString(num)).contains(Integer.toString(digit))) {
			orgNum = --num;
		if(!(Integer.toString(num)).contains(Integer.toString(digit))) {
			break;
		}
		}
		System.out.println(orgNum);
	}

}
