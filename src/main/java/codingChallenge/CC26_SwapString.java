package codingChallenge;

import java.util.Scanner;

public class CC26_SwapString {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter String1");
		String a = sc.nextLine(); 
		System.out.println("Enter String2");
		String b = sc.nextLine(); 
		
		System.out.println("Before swap a is: "+a);
		System.out.println("Before swap b is: "+b);
		
		a = a+b;
		
		b = a.substring(0, (a.length() - b.length()));
		a = a.substring(b.length());
		
		System.out.println("After swap a is: "+a);
		System.out.println("After swap b is: "+b);

	}

}
