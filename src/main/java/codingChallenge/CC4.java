package codingChallenge;
import java.time.Month;
import java.util.*;
public class CC4 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the month number:");
		int nextInt = sc.nextInt();
		Month month = Month.of(nextInt);
		System.out.println(month);
		System.out.println(month.length(false));
	}

}
