package projectDay;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class Zoomcar {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.get("https://www.zoomcar.com/chennai/");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		
		driver.findElementByXPath("//a[@title = 'Start your wonderful journey']").click();
		
		driver.findElementByXPath("//div[contains(text(),'Nelson Manickam Road')]").click();
		
		WebElement next = driver.findElementByClassName("proceed");
		next.click();
		
		Date date = new Date();
		DateFormat sdf = new SimpleDateFormat("dd");
		String today = sdf.format(date);
		int tomo = (Integer.parseInt(today)+1);
		System.out.println(tomo);
		
		driver.findElementByXPath("//div[contains(text(),'"+tomo+"')]").click();
		
	//	driver.findElementByClassName("//div[@class = 'heading']")
		
		next.click();
		
		driver.findElementByXPath("//button[text()='Done']").click();
		
		driver.findElementByXPath("//div[contains(text(),'High to Low')]");
		
		List<WebElement> allPrices = driver.findElementsByXPath("(//div[@class = 'price'])");
		//allPrices.findElements(By.xpath(xpathExpression))
		List<String> output = new ArrayList<>();
		for (WebElement prices : allPrices) {
	//		System.out.println(prices.getText());
			String all = (prices.getText());
			String priceList = all.replaceAll("\\D", "");
			output.add(priceList);
		}
		Collections.sort(output);
		System.out.println(output);
		
		int i = output.size();
		System.out.println(output.get(i-1));
		
		WebElement carName = driver.findElementByXPath("(((//div[contains(text(), '"+output.get(i-1)+"')])/../..)/preceding-sibling::div[1])/h3");
		System.out.println(carName.getText());
	}
}


