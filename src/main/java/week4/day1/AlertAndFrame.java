package week4.day1;
import org.openqa.selenium.Alert;
import org.openqa.selenium.chrome.ChromeDriver;
import java.util.Scanner;

public class AlertAndFrame {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://www.w3schools.com/js/tryit.asp?filename=tryjs_prompt");
		
		driver.switchTo().frame("iframeResult");
		driver.findElementByXPath("//button[text() = 'Try it']").click();
		
//		Scanner sc = new Scanner(System.in);
//		String input = sc.nextLine();
//		System.out.println("Enetr the name" + input);
		
		Alert myAlert = driver.switchTo().alert();
		Scanner sc = new Scanner(System.in);
		String input = sc.nextLine();
		System.out.println("Enetr the name" + input);
	//	myAlert.sendKeys("Sathis kumar");
		myAlert.sendKeys(input);
		myAlert.accept();
		
	//	boolean display = driver.findElementByXPath("//p[contains(text(), 'Sathis kumar')]").isDisplayed();
		String text = driver.findElementByXPath("(//button[text() = 'Try it'])/following-sibling::p").getText();
	//	boolean display = driver.findElementByXPath("//p[contains(text(), 'Sathis kumar')]").isDisplayed();
		if(text.contains(input)) {
			System.out.println("Message contains  my name");
		}else {
			System.out.println("Message does not contain my name");
		}

	}

}
