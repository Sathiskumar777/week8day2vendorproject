package week4.day1.HW;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.chrome.ChromeDriver;

public class MergeLead {

	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("http://leaftaps.com/opentaps/");
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		
		driver.findElementById("username").sendKeys("DemoSalesManager");
		driver.findElementByName("PASSWORD").sendKeys("crmsfa");
		driver.findElementByClassName("decorativeSubmit").click();
		
		driver.findElementByLinkText("CRM/SFA").click();
		
		driver.findElementByLinkText("Leads").click();
		
		driver.findElementByXPath("//a[text() = 'Merge Leads']").click();
		
		driver.findElementByXPath("(//img[@alt = 'Lookup'])[1]").click();
		
		Set<String> allWindows = driver.getWindowHandles();
		List<String> ls = new ArrayList<>();
		ls.addAll(allWindows);
		driver.switchTo().window(ls.get(1));
		
		driver.findElementByName("id").sendKeys("10013");
		driver.findElementByXPath("//button[text() = 'Find Leads']").click();
		Thread.sleep(2000);
		driver.findElementByXPath("//td[@class = 'x-grid3-col x-grid3-cell x-grid3-td-partyId x-grid3-cell-first ']//a").click();
		
		driver.switchTo().window(ls.get(0));
		
		driver.findElementByXPath("(//img[@alt ='Lookup'])[2]").click();
		
		allWindows = driver.getWindowHandles();
		ls = new ArrayList<>();
		ls.addAll(allWindows);		
		driver.switchTo().window(ls.get(1));
		
		driver.findElementByName("id").sendKeys("10024");
		driver.findElementByXPath("//button[text() = 'Find Leads']").click();
		Thread.sleep(2000);
		driver.findElementByXPath("//td[@class = 'x-grid3-col x-grid3-cell x-grid3-td-partyId x-grid3-cell-first ']//a").click();
		
		driver.switchTo().window(ls.get(0));
		
		driver.findElementByLinkText("Merge").click();
		
		driver.switchTo().alert().accept();
		
		driver.findElementByLinkText("Find Leads").click();
		
		driver.findElementByName("id").sendKeys("10013");
		
		driver.findElementByXPath("//button[text() = 'Find Leads']").click();
		
		boolean displayed = driver.findElementByXPath("//div[contains(text(),'No records to display')]").isDisplayed();
		if(displayed) {
			System.out.println(driver.findElementByXPath("//div[contains(text(),'No records to display')]").getText());
		}else {
			System.out.println("No error message displayed");
		}
		driver.close();
	}

}
