/*package week4.day2;

import java.util.List;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class CommonMethods {

	public static void main(String[] args) throws IOException {
		//Launching browser
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.get("http://leaftaps.com/opentaps/");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		
		WebElement find = driver.findElementByXPath("//option[text() = 'test']");
		Select sc = new Select(find);
		sc.selectByVisibleText("   ");
		sc.selectByValue("  ");
		sc.selectByIndex(2);
		
		//getcount of option values
		java.util.List<WebElement> options = sc.getOptions();
		int count = options.size();
		
		//get the text 
		driver.findElementByXPath("//div[text() = '  ']").getText();
		
		//get the current url and title
		System.out.println(driver.getTitle());

		
		//is selected()?
		driver.findElementById(" ").isSelected();
		
		//get new window
		Set<String> windowHandles = driver.getWindowHandles();
		List<String> ls = new ArrayList<>();
		ls.addAll(windowHandles);
		driver.switchTo().window(ls.get(1));
		
		//take snapshots
		File src = driver.getScreenshotAs(OutputType.FILE);
		File dec = new File("./snaps/img.jpeg");
		FileUtils.copyFile(src,dec);
		
		//Alert methods
		driver.switchTo().alert().accept();
		driver.switchTo().alert().dismiss();
		driver.switchTo().alert().sendKeys(" ");
		String text = driver.switchTo().alert().getText();
		System.out.println(text);
		
		//Frame methods
		//by index
		driver.switchTo().frame(1);
		//by name or id
		driver.switchTo().frame(" ");
		//by webelement
		WebElement element = driver.findElementByClassName(" ");
		driver.switchTo().frame(element);
		//default content
		driver.switchTo().defaultContent();
		//Nested frame
		driver.switchTo().parentFrame();
		
		//webtable 4th row 3rd column --readtext
		//find out the table
		WebElement tablename = driver.findElementById("table1");
		//get all the rows in the taale in LIST
		List<WebElement> rows = tablename.findElements(By.tagName("tr"));
		//get all the columns in the row(tr) got above
		List<WebElement> column = rows.get(3).findElements(By.tagName("td"));
		//get the 3rd column and print
		String text2 = column.get(2).getText();
		System.out.println(text2);
		
		//Waits
		//impli wait
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		//Thread
		Thread.sleep(2000);
		//Webdriver wait
		WebDriverWait wait = new WebDriverWait(driver,10);
		WebElement elements = driver.findElementById(" ");
		wait.until(ExpectedConditions.elementToBeClickable(elements));
		
		//Locators
		driver.findElementById(" ").clear();
		driver.findElementById(" ").sendKeys(" ");
		driver.findElementByClassName(" ").getAttribute();
		driver.findElementByXPath(" ").click();
		
		
	}

}
*/