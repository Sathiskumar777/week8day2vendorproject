package week2.day2.CW;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;

public class MapPrintCW1 {

	public static void main(String[] args) {
		Map<Integer,String> obj = new LinkedHashMap<>();
		obj.put(1, "Sathis");
		obj.put(2, "Sathya");
		obj.put(3, "Subbu");
	for (Entry<Integer, String> objColl : obj.entrySet()) {
		System.out.println(objColl.getKey()+"-->"+objColl.getValue());
	}

	}

}
