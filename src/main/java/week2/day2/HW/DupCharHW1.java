package week2.day2.HW;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;

public class DupCharHW1 {
	
	public static void main (String[] args) {
		String str = "sathis";
		char[] ch = str.toCharArray();
			Map<Character,String> objColl = new LinkedHashMap<>();
			for(int i=0;i<ch.length;i++) {
				//System.out.println(ch[i]);
				if(objColl.containsKey(ch[i])) {
					objColl.put(ch[i],"Duplicate");
				}
					else {
						objColl.put(ch[i], "Not duplicate");
					}
			
				}
			for(Entry<Character,String> obj : objColl.entrySet()) {
				System.out.println(obj.getKey()+"-->"+obj.getValue());
			}
	}

}

