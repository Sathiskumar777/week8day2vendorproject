package week2.day1.HW1;
import java.util.List;
import java.util.ArrayList;
import java.util.Collections;

public class RemNameHW1c {

	public static void main(String[] args) {
		List<String> empName = new ArrayList<>();
		empName.add("Sathis");
		empName.add("Sathya");
		empName.add("Ramasamy");
		empName.add("Rukmani");
		empName.add("Madhava");
		for (String empNameColl : empName) {
			if(empNameColl.contains("d")) {
				System.out.println(empNameColl);
				System.out.println(empName.remove(empNameColl));
				Collections.sort(empName);
				System.out.println(empName);
			}
		}
		//System.out.println(empName.contains("Rukmani"));

	}

}
