package week2.day1.CW2;
import java.util.ArrayList;
import java.util.List;;
public class AddNames {

	public static void main(String[] args) {
		List<String> coll = new ArrayList<>();
		coll.add("Sathis");
		coll.add("Sathya");
		coll.add("Mazhiraruvi");
		coll.add("Tamil");
		coll.add("Subbu");
		for (String collobj : coll) {
			System.out.println(collobj);
		}		
		
		System.out.println(coll.get(2));
		System.out.println(coll.size());
		System.out.println(coll.remove("Tamil"));
		System.out.println(coll.size());
		System.out.println(coll.contains("Tamil"));
		coll.clear();
		System.out.println(coll.isEmpty());
		System.out.println(coll.size());

	}

}
