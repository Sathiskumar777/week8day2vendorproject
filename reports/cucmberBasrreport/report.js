$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("src/test/java/feature/vendor.feature");
formatter.feature({
  "name": "Print the Vendor name",
  "description": "",
  "keyword": "Feature"
});
formatter.scenario({
  "name": "Positive flow",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "Open the Browser",
  "keyword": "Given "
});
formatter.match({
  "location": "VendorName.openTheBrowser()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Maximise",
  "keyword": "And "
});
formatter.match({
  "location": "VendorName.maximiseTheBrowser()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "set the Timeout",
  "keyword": "And "
});
formatter.match({
  "location": "VendorName.setTheTimeout()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Load the URL",
  "keyword": "And "
});
formatter.match({
  "location": "VendorName.loadTheURL()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Enter Username",
  "keyword": "And "
});
formatter.match({
  "location": "VendorName.enterUsername()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Enter Password",
  "keyword": "And "
});
formatter.match({
  "location": "VendorName.enterPassword()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "click Login",
  "keyword": "When "
});
formatter.match({
  "location": "VendorName.clickLogin()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Mouse over the Vendors option",
  "keyword": "And "
});
formatter.match({
  "location": "VendorName.mouseOverTheVendorsOption()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "click on Search for Vendor",
  "keyword": "When "
});
formatter.match({
  "location": "VendorName.clickOnSearchForVendor()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Enter TAXid",
  "keyword": "Then "
});
formatter.match({
  "location": "VendorName.enterTAXid()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "click on Search button",
  "keyword": "When "
});
formatter.match({
  "location": "VendorName.clickOnSearchButton()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Print the Vendor name",
  "keyword": "Then "
});
formatter.match({
  "location": "VendorName.printTheVendorName()"
});
formatter.result({
  "status": "passed"
});
});