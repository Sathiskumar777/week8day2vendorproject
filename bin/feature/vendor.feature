Feature: Print the Vendor name 

Scenario: Positive flow 

	Given Open the Browser 
	And Maximise 
	And set the Timeout 
	And Load the URL 
	And Enter Username 
	And Enter Password 
	When click Login 
	And Mouse over the Vendors option 
	When click on Search for Vendor 
	Then Enter TAXid 
	When click on Search button 
	Then Print the Vendor name 
	
